const bigNumber = require("bignum");
const snekfetch = require("snekfetch");
const env = require("./env");
const mongo = require("./mongo");

const Flake = require("flake-idgen");

const flake = new Flake({
  epoch: env.epoch
});

function generateSnowflake() {
  return new Promise(function(resolve, reject) {
    flake.next(function(err, id) {
      if (err) reject(err);
      else resolve(bigNumber.fromBuffer(id).toString());
    });
  });
}

const ALL_KEYS = [["c", "channelId"], ["g", "guildId"], ["u", "userId"]];
const ALL_KEYS_MAP = new Map(ALL_KEYS);

async function dispatch(eventName, data) {
  const underIndex = eventName.indexOf("_");
  const eventAction = eventName.substring(underIndex + 1);
  const eventType = eventName.substring(0, underIndex);

  let itemId = null;
  let key = null;

  for (const [subscriptionKey, eventKey] of ALL_KEYS) {
    if (data[eventKey]) {
      itemId = data[eventKey];
      key = subscriptionKey;
      break;
    }
  }

  const updateFlag = {
    $set: {
      dispatchId: await module.exports.generateSnowflake(),
      itemId,
      eventType,
      eventAction
    }
  };

  if (eventAction == "DELETE") {
    updateFlag.$set.payload = {
      id: itemId
    };
  } else {
    for (const key in data) {
      updateFlag.$set[`payload.${key}`] = data[key];
    }
  }

  await Promise.all([
    mongo.db.collection("events").updateOne(
      {
        subscriptionQuery: key + "_" + itemId,
        eventType,
        itemId
      },
      updateFlag,
      {
        upsert: true
      }
    ),
    mongo.db
      .collection("subscriptions")
      .findAsync({
        subscriptionQuery: key + "_" + itemId,
        active: true
      })
      .then(async subscriptions => {
        const hostAddresses = new Map();
        for await (const subscription of subscriptions) {
          const existing = hostAddresses.get(subscription.hostAddress);
          if (existing) existing.add(subscription.sessionId);
          else
            hostAddresses.set(
              subscription.hostAddress,
              new Set([subscription.sessionId])
            );
        }

        const promises = new Set();

        for (const [gateway, sessions] of hostAddresses.entries()) {
          console.log("Dispatching message to gw @", gateway, data, sessions);
          promises.add(
            snekfetch.post(gateway).send({
              d: [
                {
                  t: eventName,
                  d: data
                }
              ],
              r: Array.from(sessions)
            })
          );
        }
        return await Promise.all(promises.values());
      })
  ]);
}

module.exports = {dispatch, generateSnowflake};
