const http = require("http");
const https = require("https");

const dns = require("dns");

const os = require("os");

const url = require("url");
const parse5 = require("parse5");

const pkg = require("./package.json");
const utils = require("./utils");

const r = require("./rethink");
const crypto = require("crypto");

const sharp = require("sharp");
const Color = require("color");

// {[url]: {headers, statusCode, body}}

const caches = new Map();
const dnsCache = new Map();

let interfaces = os.networkInterfaces();

function wrapResolve(resolvefn) {
  return function wrappedResolve(hostname) {
    return new Promise(function wrappedResolvePromise(resolve, reject) {
      resolvefn(hostname, function resolveCallback(err, addresses) {
        if (err) reject(err);
        else resolve(addresses);
      });
    });
  };
}

const resolvers = [
  wrapResolve(dns.resolve4),
  wrapResolve(dns.resolve6),
  async function cnameWrapper(originalHostname, depth = 0) {
    if (depth >= 5) throw new Error("Recursion limit reached");
    const resolveAsync = wrapResolve(dns.resolveCname);
    const hostnames = await resolveAsync(originalHostname);

    if (!hostnames) return [];
    const hostname = hostnames[Math.floor(Math.random() * hostnames.length)];

    console.log("CNAME burrowing into", hostname);

    try {
      let addresses = await dnsCache.get(hostname);
    } catch (err) {
      console.warn(err);
      let addresses = null;
    }

    if (!addresses) {
      const addressPromise = (async () => {
        let addresses = null;
        for (const resolver of resolvers) {
          try {
            addresses = await resolver(hostname, depth + 1);
            if (addresses && addresses.length) break;
          } catch (err) {
            console.warn(err);
          }
        }
        return addresses;
      })();
      dnsCache.set(hostname, addressPromise);
      addresses = await addressPromise;
    }

    if (!addresses || !addresses.length) return [];
    else return addresses;
  }
];

setInterval(() => {
  console.warn("Janitor clearing caches");

  caches.clear();
  dnsCache.clear();

  interfaces = os.networkInterfaces();
}, 1000 * 60 * 60);

// stolen: https://github.com/frenchbread/private-ip/blob/master/src/index.js
function isPrivate(ip) {
  return (
    /^(::f{4}:)?10\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/i.test(ip) ||
    /^(::f{4}:)?192\.168\.([0-9]{1,3})\.([0-9]{1,3})$/i.test(ip) ||
    /^(::f{4}:)?172\.(1[6-9]|2\d|30|31)\.([0-9]{1,3})\.([0-9]{1,3})$/i.test(
      ip
    ) ||
    /^(::f{4}:)?127\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/i.test(ip) ||
    /^(::f{4}:)?169\.254\.([0-9]{1,3})\.([0-9]{1,3})$/i.test(ip) ||
    /^f[cd][0-9a-f]{2}:/i.test(ip) ||
    /^fe80:/i.test(ip) ||
    /^::1$/.test(ip) ||
    /^::$/.test(ip)
  );
}

const embedUserAgent = `Mozilla/5.0 (compatible; ${pkg.name}/${
  pkg.version
}; Discordbot/2.0; +${pkg.repository})`;

const proxyUserAgent = `Mozilla/5.0 (compatible; ${pkg.name}/${
  pkg.version
}; Discordmedia/2.0; +${pkg.repository})`;

function makeRequest(originalUrl, userAgent, redirectCount = 0) {
  const originalObj = url.parse(originalUrl);

  return new Promise(async (resolve, reject) => {
    if (!originalObj || !originalObj.hostname)
      return reject(new Error("Malformed URL"));

    let addresses = null;
    try {
      addresses = await dnsCache.get(originalObj.hostname);
    } catch (err) {
      console.warn(err);
    }

    if (!addresses) {
      const addressPromise = (async () => {
        let addresses = [];
        for (const resolver of resolvers) {
          try {
            console.log("Resolving", originalObj.hostname);
            addresses = await resolver(originalObj.hostname);
            if (addresses && addresses.length) break;
          } catch (err) {
            console.warn(err);
          }
        }
        return addresses;
      })();

      dnsCache.set(originalObj.hostname, addressPromise);
      addresses = await addressPromise;
    }

    console.log("Addresses", addresses);

    if (!addresses || !addresses.length) {
      reject(new Error("Couldn't resolve hostname to address"));
    }

    if (
      addresses.some(
        address =>
          Object.values(interfaces).some(addresses =>
            addresses.some(addressObject => address == addressObject.address)
          ) || isPrivate(address)
      )
    ) {
      reject(new Error("Address resolves back to us, ignoring"));
    }

    const address = addresses[Math.floor(Math.random() * addresses.length)];

    console.log("Address chosen", address);

    return (originalObj.protocol == "https:" ? https : http)
      .request(
        {
          headers: {
            Host: originalObj.hostname,
            "User-Agent": userAgent
          },
          host: address, // originalObj.hostname,
          path: originalObj.path,
          port: originalObj.port,
          protocol: originalObj.protocol
        },
        response => {
          console.log("GOT A RESP");
          response._urlObj = originalObj;
          // console.log(response);
          if (
            response.statusCode == 301 ||
            response.statusCode == 302 ||
            response.statusCode === 307 ||
            response.statusCode == 308 ||
            response.statusCode == 303
          ) {
            if (redirectCount >= 10)
              reject(new Error("Maximum recursion depth reached"));
            const newTarget = originalObj.resolve(response.headers.location);

            const cached = caches.get(originalUrl);
            if (cached) caches.set(newTarget, cached);

            console.log("Following redirect:", newTarget);
            makeRequest(newTarget, userAgent, ++redirectCount)
              .then(resolve)
              .catch(reject);
          } else {
            resolve(response);
          }
        }
      )
      .on("error", err => reject(err))
      .end();
  });
}

function missingTag(res, tagName) {
  const err = `Missing tag ${tagName}`;
  console.log(err);

  res.writeHead(422, {
    "Content-Type": "text/plain",
    "Content-Length": Buffer.byteLength(err)
  });
  res.end(err);
}

// Must be seperated because we don't want just anyone to be able to add embeds!
http
  .createServer(async function(req, res) {
    if (req.url == "/embed") {
      try {
        var data = await new Promise((resolve, reject) => {
          let dataBody = "";

          req
            .on("data", function(data) {
              dataBody += data;
            })
            .on("end", function() {
              try {
                resolve(JSON.parse(dataBody));
              } catch (err) {
                reject(err);
              }
            });
        });
      } catch (err) {
        const {stack} = err;
        res.writeHead(422, {
          "Content-Type": "text/plain",
          "Content-Length": Buffer.byteLength(stack)
        });
        return res.end(stack);
      }

      try {
        var proxyRequest = await makeRequest(data.link, embedUserAgent);
      } catch (err) {
        const {stack} = err;
        console.log("Bad proxy request", stack);
        res.writeHead(500, {
          "Content-Type": "text/plain",
          "Content-Length": Buffer.byteLength(stack)
        });
        res.end(stack);
        return;
      }

      const embed = {};

      if (
        proxyRequest.headers["content-type"] &&
        proxyRequest.headers["content-type"].startsWith("image/")
      ) {
        const transform = sharp();
        try {
          const metadata = await new Promise((resolve, reject) => {
            transform.on("error", function(err) {
              reject(err);
            });

            proxyRequest.pipe(transform);

            transform
              .metadata()
              .then(resolve)
              .catch(reject);
          });
          embed.image = proxyRequest._urlObj.href;
          embed.height = metadata.height;
          embed.width = metadata.width;
        } catch (err) {
          console.log("Crashed on embedding img?", stack);
          res.writeHead(422);
          res.end();
        }
      } else {
        const proxyUrlObj = proxyRequest._urlObj;

        const reqBody = await new Promise((resolve, reject) => {
          const buffers = [];

          proxyRequest.on("data", function(data) {
            buffers.push(data);
          });
          proxyRequest.on("end", function() {
            resolve(Buffer.concat(buffers).toString("utf8"));
          });
        });

        try {
          var document = parse5.parse(reqBody);
        } catch (err) {
          console.warn(err);
          res.writeHead(422, {
            "Content-Length": Buffer.byteLength(err.stack),
            "Content-Type": "text/plain"
          });
          return res.end(err.stack);
        }

        const html = document.childNodes.find(node => node.nodeName == "html");

        if (!html) return missingTag(res, "html");

        const head = html.childNodes.find(node => node.nodeName == "head");
        if (!head) return missingTag(res, "head");

        for (const tag of head.childNodes) {
          if (tag.nodeName != "meta") continue;

          let foundValue = null;
          let foundProvider = null;
          let foundProperty = null;

          for (const attribute of tag.attrs) {
            if (attribute.name == "property" || attribute.name == "name") {
              if (attribute.value.startsWith("og:")) {
                foundProvider = "ogp";
                foundProperty = attribute.value.substring(3);
              } else if (attribute.value.startsWith("twitter:")) {
                foundProvider = "twitter";
                foundProperty = attribute.value.substring(8);
              } else if (attribute.value == "theme-color") {
                foundProperty = attribute.value;
                foundProvider = "generic";
              }
            } else if (attribute.name == "content") {
              foundValue = attribute.value;
            } else {
            }

            if (foundValue && foundProperty) {
              if (foundProvider == "twitter") {
                if (foundProperty == "description") {
                  embed.description = foundValue;
                } else if (foundProperty == "title") {
                  embed.title = foundValue;
                } else if (foundProperty == "image") {
                  embed.image = proxyUrlObj.resolve(foundValue);
                } else if (foundProperty == "player") {
                  embed.player = proxyUrlObj.resolve(foundValue);
                } else if (foundProperty == "player:height") {
                  embed.height = foundValue;
                } else if (foundProperty == "player:width") {
                  embed.width = foundValue;
                } else {
                  console.log("Twitter tag unhandled", foundProperty);
                }
              } else if (foundProvider == "ogp") {
                if (foundProperty == "description") {
                  embed.description = foundValue;
                } else if (foundProperty == "title") {
                  embed.title = foundValue;
                } else if (
                  foundProperty == "image" ||
                  foundProperty == "image:url" ||
                  foundProperty == "image:secure_url"
                ) {
                  embed.image = proxyUrlObj.resolve(foundValue);
                } else if (foundProperty == "url") {
                  // Ehhh... this seems like it will just cause issues
                  // embed.url = foundValue;
                } else if (
                  foundProperty == "video" ||
                  foundProperty == "video:url" ||
                  foundProperty == "video:secure_url"
                ) {
                  embed.player = proxyUrlObj.resolve(foundValue);
                } else if (
                  foundProperty == "image:height" ||
                  foundProperty == "video:height"
                ) {
                  const num = Number(foundValue);
                  if (num) embed.height = num;
                } else if (
                  foundProperty == "image:width" ||
                  foundProperty == "video:width"
                ) {
                  const num = Number(foundValue);
                  if (num) embed.width = num;
                } else {
                  console.log("OGP tag unhandled!", foundProperty);
                }
              } else if (foundProvider == "generic") {
                if (foundProperty == "theme-color") {
                  try {
                    embed.color = Color(foundValue).rgbNumber();
                  } catch (err) {
                    console.warn("Error parsing color", err);
                  }
                }
              }

              break;
            }
          }
        }
      }

      if (embed.image && (!embed.height || !embed.width)) {
        const imageReq = await makeRequest(embed.image, embedUserAgent);

        const transform = sharp();
        try {
          const metadata = await new Promise((resolve, reject) => {
            transform.on("error", function(err) {
              reject(err);
            });

            imageReq.pipe(transform);

            transform
              .metadata()
              .then(resolve)
              .catch(reject);
          });
          embed.height = metadata.height;
          embed.width = metadata.width;
        } catch (err) {
          console.log("threw on embedding img?", stack);
          delete embed.image;
          delete embed.height;
          delete embed.width;
        }
      }

      if (Object.keys(embed).length !== 0) {
        console.log("Generated a good embed", embed);
        const linkHash = crypto
          .createHash("sha512")
          .update(Buffer.from(data.link, "utf8"))
          .digest("base64");

        const generatedId = `${data.messageId}-${linkHash}`;

        await r
          .table("embeds")
          .insert({
            id: generatedId,
            messageId: data.messageId,
            link: data.link,
            ...embed
          })
          .run(r.conn);

        await utils.dispatch("EMBED_CREATE", {
          messageId: data.messageId,
          guildId: data.guildId,
          channelId: data.channelId,
          link: data.link,
          id: generatedId,
          ...embed
        });
      } else {
        console.log("Yielded no embed");
      }
      res.writeHead(204);
      res.end();
    } else {
      res.writeHead(404);
      res.end("what.");
    }
  })
  .listen(3021);

http
  .createServer(async function(req, res) {
    res.setHeader("Cache-Control", "public, max-age=86400");
    console.log(req.url);
    if (!req.url.startsWith("/proxy?")) {
      res.writeHead(404);
      return res.end();
    }

    try {
      var pageUrl = decodeURIComponent(req.url.substring(7));
    } catch (err) {
      res.writeHead(422);
      return res.end();
    }

    console.log(pageUrl);

    const cachedRequestPromise = caches.get(pageUrl);
    if (cachedRequestPromise) {
      const cachedRequest = await cachedRequestPromise;
      if (cachedRequest.statusCode != 200) {
        res.writeHead(cachedRequest.statusCode);
        return res.end();
      }
      res.writeHead(200, cachedRequest.headers);

      if (cachedRequest.stream) {
        console.log("Stream still attached");
        cachedRequest.stream.pipe(res);
      } else {
        console.log("Ending");
        res.end(cachedRequest.data);
      }

      return;
    }

    caches.set(
      pageUrl,
      makeRequest(pageUrl, proxyUserAgent)
        .then(async proxying => {
          const cacheContainer = {
            headers: null,
            statusCode: proxying.statusCode,
            data: null,
            stream: proxying
          };

          if (proxying.statusCode != 200) {
            res.writeHead(proxying.statusCode);
            res.end();
            return cacheContainer;
          }

          const isSVG =
            proxying.headers["content-type"] &&
            proxying.headers["content-type"].includes("svg");

          cacheContainer.headers = {
            "Content-Type": isSVG
              ? proxying.headers["content-type"]
              : "image/png"
          };
          if (isSVG) {
            if (proxying.headers["content-length"])
              cacheContainer.headers["Content-Length"] =
                proxying.headers["content-length"];
          }

          const buffers = [];

          if (isSVG) {
            res.writeHead(200, cacheContainer.headers);
            cacheContainer.stream = proxying;
          } else {
            const transformer = sharp()
              .on("error", function(err) {
                cacheContainer.statusCode = 422;
                cacheContainer.data = null;
                res.writeHead(422);

                res.end();
              })
              .on("info", function(info) {
                res.writeHead(200, cacheContainer.headers);
                if (info.height > 420 * 2 || info.width > 200 * 2)
                  return this.resize(420 * 2, 200 * 2, {fit: "inside"});
                else return this;
              })
              .png();

            cacheContainer.stream = transformer;

            proxying.pipe(transformer);
          }

          cacheContainer.stream.pipe(res);
          cacheContainer.stream.on("data", function(dataChunk) {
            buffers.push(dataChunk);
          });

          cacheContainer.stream.on("end", function() {
            cacheContainer.data = Buffer.concat(buffers);
            console.log("Proxy req ended");
            delete cacheContainer.stream;
            cacheContainer.stream = null;
            delete cacheContainer.stream;
          });

          return cacheContainer;
        })
        .catch(err => {
          console.log(err);
          res.writeHead(422);
          res.end();
          return {
            statusCode: 422
          };
        })
    );
  })
  .listen(3022);

console.log("y");
