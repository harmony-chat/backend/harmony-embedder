module.exports = {
  mongo: {
    db: process.env.MONGO_DB || "harmony-state",
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || "27017"
  },
  deployment: process.env.NODE_ENV || "local",
  stateServerMap: process.env.STATE_SERVER_PREFIX
    ? new Array(process.env.STATE_SERVERS)
        .fill(null)
        .map(
          (_, index) => process.env.STATE_SERVER_PREFIX + (index + 1) + ":3002"
        )
    : process.env.STATE_SERVERS
      ? process.env.STATE_SERVERS.split(",")
      : ["localhost:3002"],

  epoch: (process.env.EPOCH
    ? new Date(Date.UTC(process.env.EPOCH))
    : new Date(Date.UTC(2018, 1, 1))
  ).getTime(),

  rethink: {
    host: process.env.RETHINK_HOST || "localhost",
    port: process.env.RETHINK_PORT || 28015,
    user: process.env.RETHINK_USER || "admin",
    password: process.env.RETHINK_PASSWORD || "",
    get db() {
      return `harmony_${module.exports.deployment}`;
    }
  }
};
